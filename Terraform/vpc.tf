########################################
### VPC Configs: #######################
########################################

resource "aws_vpc" "aztek" {
    cidr_block  = var.vpc["vpc_cidr"]
    tags        = merge(
        {
            Name = join("-", [var.global["project"], var.environment])
        },
        local.tags
    )

    enable_dns_hostnames    = true
}

########################################
### Internet Gateways ##################
########################################

resource "aws_internet_gateway" "aztek" {
    vpc_id  = aws_vpc.aztek.id
    tags    = merge(
        {
            Name = join("-", [var.global["project"], var.environment])
        },
        local.tags
    )
}

########################################
### Subnets ############################
########################################

resource "aws_subnet" "public" {
    count               = length(data.aws_availability_zones.available.names)
    vpc_id              = aws_vpc.aztek.id
    cidr_block          = var.vpc["pub_ws_subnet_${count.index}"]
    availability_zone   = data.aws_availability_zones.available.names[count.index]
    tags                = merge(
        {
            Public  = true
            Name    = join("-", [var.global["project"], var.environment, "public-subnet", count.index])
        },
        local.tags
    )
}

########################################
### Route Tables #######################
########################################

# Public
####################

resource "aws_default_route_table" "public_web_server" {
    default_route_table_id  = aws_vpc.aztek.default_route_table_id

    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.aztek.id
    }

    tags = merge(
        {
            Name = join("-", [var.global["project"], var.environment, "default-route-table"])
        },
        local.tags
    )
}

########################################
### NAT Gateway ########################
########################################

# This is for the lambda function efs_backup
# Refer here if you have questions:
#     http://docs.aws.amazon.com/lambda/latest/dg/vpc.html#vpc-internet

/*
resource "aws_eip" "nat_ip" {
    count = length(data.aws_availability_zones.available.names)
}

resource "aws_nat_gateway" "private" {
    count           = length(data.aws_availability_zones.available.names)
    allocation_id   = aws_eip.nat_ip[count.index].id
    subnet_id       = aws_subnet.private[count.index].id
}
*/

########################################
### Security Groups ####################
########################################

resource "aws_default_security_group" "default" {
    vpc_id = aws_vpc.aztek.id

    tags    = merge(
        {
            Name = join("-", [var.global["project"], var.environment, "defualt"])
        },
        local.tags
    )

    ingress {
        protocol  = -1
        self      = true
        from_port = 0
        to_port   = 0
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "web_inbound" {
    vpc_id  = aws_vpc.aztek.id

    tags    = merge(
        {
            Name = join("-", [var.global["project"], var.environment, "web-services"])
        },
        local.tags
    )

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
    }

    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }

    egress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
    }
}

resource "aws_security_group" "ssh_inbound" {
    vpc_id  = aws_vpc.aztek.id
    tags    = merge(
        {
            Name = join("-", [var.global["project"], var.environment, "ssh-trusted"])
        },
        local.tags
    )

    ingress {
        cidr_blocks = [
            "${var.home_ip}/32",
            "${var.work_ip}/32"
        ]
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
    }

    egress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
    }
}

resource "aws_security_group" "web_inbound_trusted" {
    vpc_id  = aws_vpc.aztek.id
    tags    = merge(
        {
            Name = join("-", [var.global["project"], var.environment, "web-services-trusted"])
        },
        local.tags
    )

    ingress {
        cidr_blocks = [
            "${var.home_ip}/32",
            "${var.work_ip}/32"
        ]
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
    }

    ingress {
        cidr_blocks = [
            "${var.home_ip}/32",
            "${var.work_ip}/32"
        ]
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
    }

    egress {
        cidr_blocks = ["0.0.0.0/0"]
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
    }
}

