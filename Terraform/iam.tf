########################################
### IAM Policy Documents ###############
########################################

# Associate IP
####################

data "aws_iam_policy_document" "associate_ip" {
    statement {
        effect = "Allow"
        actions = [
            "ec2:AssociateAddress"
        ]
        resources = [
            "*"
        ]
    }
}

# Get Secret from SSM
####################

data "aws_iam_policy_document" "get_secret" {
    statement {
        effect = "Allow"
        actions = [
            "ssm:GetParameter"
        ]
        resources = [
            "*"
        ]
    }
}

########################################
### IAM Policies #######################
########################################

# Associate IP
####################

resource "aws_iam_policy" "associate_ip" {
    name    = join("-", [var.global["project"], var.environment, "AssociateIP"])
    policy  = data.aws_iam_policy_document.associate_ip.json
}

# Get Secret from SSM
####################

resource "aws_iam_policy" "get_secret" {
    name    = join("-", [var.global["project"], var.environment, "GetSecretSSM"])
    policy  = data.aws_iam_policy_document.get_secret.json
}

########################################
### IAM Roles ##########################
########################################

# Associate IP
####################

resource "aws_iam_role" "associate_ip" {
    name                = join("-", [var.global["project"], var.environment, "AssociateIP"])
    assume_role_policy  = "${lookup(var.ec2,"policy")}"
}

# Get Secret from SSM
####################

resource "aws_iam_role" "get_secret" {
    name                = join("-", [var.global["project"], var.environment, "GetSecretSSM"])
    assume_role_policy  = "${lookup(var.ec2,"policy")}"
}

########################################
### IAM Policy Attachments #############
########################################

# Associate IP
####################

resource "aws_iam_role_policy_attachment" "associate_ip" {
    role        = aws_iam_role.associate_ip.name
    policy_arn  = aws_iam_policy.associate_ip.arn
}

# Get Secret from SSM
####################

resource "aws_iam_role_policy_attachment" "get_secret" {
    role         = aws_iam_role.get_secret.name
    policy_arn   = aws_iam_policy.get_secret.arn
}

########################################
### IAM Instance Profile ###############
########################################

# Associate IP
####################

resource "aws_iam_instance_profile" "associate_ip" {
    name = join("-", [var.global["project"], var.environment, "AssociateIP"])
    role = "${aws_iam_role.associate_ip.name}"
}

# Get Secret from SSM
####################

resource "aws_iam_instance_profile" "get_secret" {
    name = join("-", [var.global["project"], var.environment, "Get_Secret_SSM"])
    role = "${aws_iam_role.get_secret.name}"
}

